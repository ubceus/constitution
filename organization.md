---
title: "2. Organization of the Society"
date: 2017-10-16T21:56:45-07:00
draft: false
---

## 2.1. Levels of Structure

The Society shall include the following structures:

1. EUS Board of Governors  

2. EUS Council

3. EUS Executive Officers

4. EUS Directors

5. EUS Managers

6. EUS Representatives

7. EUS Members at Large

8. AMS Engineering Representatives

## 2.2. The Board of Governors

The EUS Board of Governors shall:

1. Be the highest governing body within the Society, second only to a referendum of all Active Members.

2. Establish and approve all policies within the Policy Manual.

3. Exercise no control over the activities, conduct or business of any of the Program Clubs, which, although they have representation on the Board, operate under a separate charter from the UBC Alma Mater Society.

4. Allow each member one vote on each motion brought before the Board.

The EUS Board of Governors shall consist of:

1. The Executive Officers

2. The President or representative of each of the Program Clubs, as described in the Policy Manual.

Further details surrounding the appointment, selection, duties, benefits, and other conditions or requisites of members of the Board shall be set out in the Policy Manual.

## 2.3. The Council

The EUS Council shall:

1. Advise the Executive and the Board on matters brought to discussion before them.

The EUS Council shall consist of:

1. Members of the EUS Board of Governors

2. AMS Engineering Representatives

3. Other members, as defined in the Policy Manual

Further details surrounding the appointment, selection, duties, benefits, and other conditions or requisites of members of the Council shall be set out in the Policy Manual.

## 2.4. Executive Officers

Each EUS Executive Officer shall:

1. Be elected through a secret-ballot vote open to all Active EUS members. Further details surrounding elections shall be outlined in the EUS Policy Manual.  

2. Be an active member of the EUS during the term they are  serving.

3. Oversee and coordinate the Appointed Volunteers for whom they are responsible and ensure that they are performing their duties in a satisfactory manner.

4. Regularly report their activities and the activities of the Appointed Volunteers for which they are responsible for to the EUS Council.

5. Promote the Society to both the EUS members and the external community.

6. Have such other duties as are outlined in the constitution or the Policy Manual or assigned by the Board from time to time.

The Executive Officers, their duties, and their powers of the Society shall be as follows:  

### 2.4.1. President

The President shall:

1. Preside over the affairs of the Society.

2. Oversee the other members of the Executive and ensure that they are performing their duties in a satisfactory manner and as per the Board’s direction.

3. Regularly report the activities of the Executive Officers to the EUS Council.

### 2.4.2. Vice-President of Academic Affairs

The Vice-President of Academic Affairs shall:  

1. Oversee the academic services offered by the Society.

2. Be responsible for the proper representation of students at functions relevant to the engineering academic experience and curriculum.  

### 2.4.3. Vice-President of Administration

The Vice-President of Administration shall:

1. Oversee the administrative operations of the Society.

2. Assume the administrative duties of the President in the President’s absence.   

### 2.4.4. Vice-President of Communications

The Vice-President of Communications shall:

1. Oversee the communications operations of the Society.

2. Assume the public speaking duties of the President in the President’s absence.   

### 2.4.5. Vice-President of Finance

The Vice-President Finance shall:

1. Oversee the financial operations and businesses of the Society.

### 2.4.6. The Vice-President of Student Life

The Vice-President of Student Life shall:

1. Oversee and promote the social and cultural activities of the Society.

### 2.4.7. The Vice-President of Spirit

The Vice-President of Spirit shall:

1. Oversee the traditional engineering events of the Society.

2. Promote and develop engineering spirit and community on Campus.

## 2.5. Directors

Each EUS Director shall:

1. Upon recommendation by the Executive Officers, be appointed by the Board of Governors.   

2. Be assigned to an Executive Officer from whom they shall receive direction.

3. Coordinate and administer the activities charged to them.   

4. Oversee and coordinate the Managers and Representatives for whom they are responsible and ensure that they are performing their duties in a satisfactory manner.  

5. Regularly report their activities and the activities of the Managers and Representatives for whom they are responsible to their assigned Executive Officer.

6. Promote the Society to both the EUS members and the external community.

Further details surrounding the appointment, selection, duties, benefits, and other conditions or requisites of Directors shall be set out in the Policy Manual.   

## 2.6. Managers

Each EUS Manager shall:

1. Upon recommendation by the Executive Officers, be appointed by the Board of Governors.   

2. Be assigned to an Executive Officer from whom they shall report to and receive direction from.

3. May be assigned to a Director who they will receive direction and report to in lieu of the Executive Officer.

4. Oversee and coordinate the Representatives for whom they are responsible and ensure that they are performing their duties in a satisfactory manner.  

5. Regularly report their activities and the activities of the Representatives for whom they are responsible to their assigned Executive Officer.

6. Coordinate and administer the activities charged to them.   

7. Promote the Society to both the EUS members and the external community.

Further details surrounding the appointment, selection, duties, benefits, and other conditions or requisites of Managers shall be set out in the Policy Manual.  

## 2.7. Representatives

Each EUS Representative shall:

1. Be assigned to an Executive Officer from whom they shall receive direction and to whom they shall report.

2. Be appointed by the Executive Officer to whom that Representative is responsible in conjunction with the Executive Officer’s associated Directors or Managers.

3. May be assigned to a Director or Manager from whom they shall receive direction and to whom they shall report. in lieu of the Executive Officer.

4. Coordinate and administer the activities charged to them.   

5. Promote the Society to both the EUS members and the external community.

Further details surrounding the appointment, selection, duties, benefits, and other conditions or requisites of Representatives shall be set out in the Policy Manual.  

## 2.8. AMS Engineering Representatives

The AMS Engineering Representatives shall:

1. Be one Executive Officer as outlined in the Policy Manual.  

2. Be elected by a secret-ballot vote open to all Active EUS Members to fill the remaining AMS Engineering Representatives.

Further details surrounding the appointment, selection, duties, benefits, and other conditions or requisites of AMS Engineering Representatives shall be set out in the Policy Manual.   

## 2.9. EUS Members at Large

EUS Members at Large shall be all Active EUS Members who do not hold any other EUS position described in this Constitution.

## 2.10. Quorum

Quorum shall be:

1. One-twentieth (1/20) of the active members of the Society at General and Extraordinary Meetings.

2. One-tenth of the active members of the Society in referenda.

3. Three-quarters of the members at Meetings of the Board

4. Two-thirds of the Executive at Meetings of the Executive.

Further details surrounding the various meetings shall be set out in the Policy Manual.   

## 2.11. Recall of Elected Representatives

### 2.11.1. Executive Officers

An Executive Officer may be removed from office for any of the following reasons:  

1. Resignation  

2. Absence from two consecutive Council Meetings, without justification deemed valid by the Board of Governors

3. Unsatisfactory performance according to a petition signed by three-quarters of the Board of Governors or one-tenth of the active membership.

The vacancy thus created shall be temporarily filled by a member appointed by the Board until it is permanently filled through a by-election in accordance with procedures outlined in the EUS policy manual.

### 2.11.2. Non-Executive Governor on the Board of Governors

A member of the Board of Governors who is not an Executive Officer may be removed from office for any of the following reasons:  

1. Resignation  

2. Absence from two consecutive Council Meetings, without providing a proxy and without justification deemed valid by the Board of Governors.

3. Unsatisfactory performance according to a petition signed by three-quarters of the Board of Governors or one-quarter of the active membership that member of the Board represents.  

The vacancy thus created will be filled by the Program Club whose members were formerly represented by the removed Governor.

### 2.11.3. AMS Engineering Representatives

An AMS Engineering Representative may be removed from office for any of the following reasons:  

1. Resignation  

2. Absence from AMS Council Meetings  as outlined by the AMS.

3. Absence from two consecutive EUS Council Meetings, without providing a proxy and without justification deemed valid by the EUS Board.

4. Unsatisfactory performance according to a petition signed by three-quarters of the Board of Governors or one-quarter of the active membership.  

The vacancy thus created shall be temporarily filled by a member appointed by the EUS Board until it is permanently filled through a by-election in accordance with procedures outlined in the EUS Policy anual.
