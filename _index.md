---
title: "Constitution"
date: 2017-10-16T21:40:50-07:00
draft: false
# Set the page as a chapter, changing the way it's displayed
chapter: true
---

# The EUS Constitution

This is the EUS Constitution, which defines the society as a constituent of the AMS. If you have any questions or concerns, please email [vpadmin@ubcengineers.ca](mailto:vpadmin@ubcengineers.ca).
