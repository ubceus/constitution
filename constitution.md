---
title: "3. Constitution of the Society"
date: 2017-10-16T21:55:45-07:00
draft: false
---

## 3.1. Amendments

The Constitution of the Society may be amended by two-thirds approval at a General Meeting or through a referendum provided that written notification of such amendment is posted at least 10 business days prior to the referendum.

## 3.2. Warning

No part of this Constitution may be interpreted in a sense contrary to the AMS Constitution, Bylaws, and Code.
