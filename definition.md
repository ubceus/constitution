---
title: "1. Definition of the Society"
date: 2017-10-16T21:57:45-07:00
draft: false
---

## 1.1 Name 	

The name of the organization shall be "The Engineering Undergraduate Society of the University of British Columbia Vancouver".  

## 1.2. Mission

The mission of the organization is to support the academic, professional, and social needs of engineering students, encourage excellence in all aspects of student life, and celebrate the accomplishments of its members.  

Without limiting the generality of the above mission statement, the objectives of the organizations shall be:

1. To maintain communication between student groups within the engineering community.

2. To support the initiatives of student groups within the engineering community.

3. To support the social needs of its members by organizing and promoting social events and services.

4. To support the academic needs of its members through representation to relevant groups and development and promotion of academic services and events.

5. To support the professional development of its members through representation to relevant groups and development and promotion of professional services and events.

6. To support the external community by organizing and promoting events in co-operation with external groups, including events in support of charities.

## 1.3. Interpretation and Definitions

Further definitions and guidelines are detailed in the EUS Policy Manual.   

In this constitution, unless otherwise specified:

1. "UBC" shall hereafter refer to “the University of British Columbia Vancouver”.

2. "The Society" shall hereafter refer to “the Engineering Undergraduate Society of UBC”.

3. "EUS" shall be the equivalent of “the Society”

4. "Senate" shall hereafter refer to “the Senate of UBC Vancouver”

5. "Members" shall hereafter refer to “EUS members”

6. "Executive" shall be the equivalent of “Executive Officers of the Society”   

7. "The Board" shall hereafter refer to “the EUS Board of Governors ”

8. "AMS" shall hereafter refer to “the Alma Mater Society of UBC”

9. "Constitution" shall hereafter refer to “Constitution of the EUS”

10. "Policy Manual" shall hereafter refer to “Policy Manual of the EUS”

11. "Appointed Volunteers" shall hereafter refer to “Directors, Managers and Representatives of the EUS”

12. "Program Clubs" shall hereafter refer to all “Department or Program Clubs.”

## 1.4. Membership

Members shall include the following:

1. Active

    1. The only active members shall be Undergraduate Engineering students who have paid their EUS student fees.

2. Honorary

    2. Details surrounding the appointment, selection, duties, benefits, and other conditions or requisites of honorary members shall be set out in the Policy Manual.

## 1.5. Policy Manual

The EUS Policy Manual shall:

1. Require a 2/3 (two-thirds) in favour vote by the Board of Governors for the adoption of any changes.

2. Support and provide clarification of the EUS Constitution. No part of the Policy Manual may be interpreted in a sense contrary to the EUS Constitution.

Further details surrounding changes to policies within the Policy Manual can be found in the Policy Manual.
